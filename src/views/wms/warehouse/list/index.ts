import { reactive } from 'vue'
import { useHttpRequest } from '@/api/chunfeng/apiRequest'
import { QueryWarehouseParam, WarehouseData } from '@/api/chunfeng/wms/warehouse/interfaces'
import { useWarehouseApi } from '@/api/chunfeng/wms/warehouse'

const { executeQueryRequest, executeCommandRequest } = useHttpRequest()

export const useWarehouseListFunctions = () => {
  const queryParam = reactive<QueryWarehouseParam>({})
  const resultDataSet = reactive<Array<WarehouseData>>([])
  const { queryWarehouse, enableWarehouse, disableWarehouse } = useWarehouseApi()

  const columns = [
    {
      title: '名称',
      dataIndex: 'name',
      key: 'name',
    },
    {
      title: '地址',
      dataIndex: 'address',
      key: 'address',
    },
    {
      title: '备注',
      dataIndex: 'remark',
      key: 'remark',
    },
    {
      title: '状态',
      dataIndex: 'enabled',
      key: 'enabled',
      slots: { customRender: 'enabled' },
    },
    {
      title: '操作',
      key: 'action',
      slots: { customRender: 'action' },
    },
  ]

  function enable(uid: string) {
    executeCommandRequest(enableWarehouse(uid), {
      message: '仓库启用成功',
      callback: () => {
        query()
      },
    })
  }

  function disable(uid: string) {
    executeCommandRequest(disableWarehouse(uid), {
      message: '仓库停用成功',
      callback: () => {
        query()
      },
    })
  }

  function edit(uid: string) {}

  function reset() {
    queryParam.name = ''
    queryParam.enabled = undefined
  }

  function query() {
    resultDataSet.length = 0
    executeQueryRequest(queryWarehouse(queryParam), (results: WarehouseData[]) => {
      resultDataSet.push(...results)
    })
  }

  return {
    queryParam,
    resultDataSet,
    columns,
    enable,
    disable,
    query,
    reset,
    edit,
  }
}
