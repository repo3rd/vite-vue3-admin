import { reactive } from 'vue'
import { useHttpRequest } from '@/api/chunfeng/apiRequest'
import { Form } from 'ant-design-vue'
import { useWarehouseApi } from '@/api/chunfeng/wms/warehouse'
import { NewWarehouseParam } from '@/api/chunfeng/wms/warehouse/interfaces'

const { executeCommandRequest } = useHttpRequest()
const { newWarehouse } = useWarehouseApi()

export const useWarehouseNewFunctions = () => {
  const newWarehouseParam = reactive<NewWarehouseParam>({
    name: '',
    address: '',
    remark: undefined,
  })

  const rules = reactive({
    name: [
      {
        required: true,
        message: '请输入仓库名称',
      },
    ],
    address: [
      {
        required: true,
        message: '请输入仓库地址',
      },
    ],
    remark: [
      {
        required: false,
      },
    ],
  })

  const layout = {
    labelCol: { span: 4 },
    wrapperCol: { span: 14 },
  }

  const { validate, resetFields, validateInfos } = Form.useForm(newWarehouseParam, rules)

  function add() {
    validate<NewWarehouseParam>().then((data) => {
      executeCommandRequest(newWarehouse(data), {
        message: '添加仓库成功',
        callback: () => {
          resetFields()
        },
      })
    })
  }

  return {
    newWarehouseParam,
    add,
    rules,
    validateInfos,
    resetFields,
    layout,
  }
}
