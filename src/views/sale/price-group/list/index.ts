import { reactive } from 'vue'
import { PriceGroupData, QueryPriceGroupParam } from '@/api/chunfeng/sale/price-group/interfaces'
import { useHttpRequest } from '@/api/chunfeng/apiRequest'
import { usePriceGroupApi } from '@/api/chunfeng/sale/price-group'

const { executeQueryRequest, executeCommandRequest } = useHttpRequest()

export const usePriceGroupListFunctions = () => {
  const queryPriceGroupParam = reactive<QueryPriceGroupParam>({})
  const priceGroupDataSet = reactive<Array<PriceGroupData>>([])
  const { queryPriceGroup, enablePriceGroup, disablePriceGroup } = usePriceGroupApi()

  const columns = [
    {
      title: '名称',
      dataIndex: 'name',
      key: 'name',
    },

    {
      title: '状态',
      dataIndex: 'enabled',
      key: 'enabled',
      slots: { customRender: 'enabled' },
    },
    {
      title: '操作',
      key: 'action',
      slots: { customRender: 'action' },
    },
  ]

  function enable(uid: string) {
    executeCommandRequest(enablePriceGroup(uid), {
      message: '价格组启用成功',
      callback: () => {
        query()
      },
    })
  }

  function disable(uid: string) {
    executeCommandRequest(disablePriceGroup(uid), {
      message: '价格组停用成功',
      callback: () => {
        query()
      },
    })
  }

  function edit(uid: string) {}

  function reset() {
    queryPriceGroupParam.name = ''
    queryPriceGroupParam.enabled = undefined
  }

  function query() {
    priceGroupDataSet.length = 0
    executeQueryRequest(queryPriceGroup(queryPriceGroupParam), (results: PriceGroupData[]) => {
      priceGroupDataSet.push(...results)
    })
  }

  return {
    queryPriceGroupParam,
    priceGroupDataSet,
    columns,
    enable,
    disable,
    query,
    reset,
    edit,
  }
}
