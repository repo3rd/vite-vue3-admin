import { reactive } from 'vue'
import { AddPriceGroupParam } from '@/api/chunfeng/sale/price-group/interfaces'
import { useHttpRequest } from '@/api/chunfeng/apiRequest'
import { usePriceGroupApi } from '@/api/chunfeng/sale/price-group'
import { Form } from 'ant-design-vue'

const { executeCommandRequest } = useHttpRequest()
const { newPriceGroup } = usePriceGroupApi()

export const usePriceGroupAddFunctions = () => {
  const addPriceGroupParam = reactive<AddPriceGroupParam>({
    name: '',
  })

  const rules = reactive({
    name: [
      {
        required: true,
        message: '请输入品牌名称',
      },
    ],
  })

  const { validate, resetFields, validateInfos } = Form.useForm(addPriceGroupParam, rules)

  function add() {
    validate<AddPriceGroupParam>().then((data) => {
      executeCommandRequest(newPriceGroup(data), {
        message: '添加价格组成功',
        callback: () => {
          resetFields()
        },
      })
    })
  }

  return {
    addPriceGroupParam,
    add,
    rules,
    validateInfos,
    resetFields,
  }
}
