import { computed, reactive, ref, toRefs, watch } from 'vue'
import { useHttpRequest } from '@/api/chunfeng/apiRequest'
import { message } from 'ant-design-vue'
import { usePriceApi } from '@/api/chunfeng/sale/price'
import { SimpleUnitData } from '@/api/chunfeng/unit/interfaces'
import { SimpleCategoryData } from '@/api/chunfeng/category/interfaces'
import { SimpleBrandData } from '@/api/chunfeng/brand/interfaces'

import { useCustomerProductPriceApi } from '@/api/chunfeng/sale/customer-price'

import { ProductPriceData } from '@/api/chunfeng/sale/price/interfaces'

import { PriceGroupData } from '@/api/chunfeng/sale/price-group/interfaces'

const { executeCommandRequest, executeQueryRequest } = useHttpRequest()
const { queryProductPrice, batchAddPrice } = usePriceApi()

const { queryCustomerProductPrice } = useCustomerProductPriceApi()

export const useCustomerPriceAddFunctions = () => {
  const customerIdRef = ref('')

  interface CustomerPriceParam {
    productPriceDataSet: ProductPriceData[]
    priceGroupDataSet: PriceGroupData[]
  }

  const customerPriceParamRef = reactive<CustomerPriceParam>({
    productPriceDataSet: [],
    priceGroupDataSet: [],
  })

  function customerSelected(customerId: string) {
    customerIdRef.value = customerId
  }

  watch(
    () => customerIdRef,
    (newVal) => {
      loadCustomerProductPrice(newVal.value)
    },
  )

  function loadCustomerProductPrice(customerId: string) {
    // executeQueryRequest(queryCustomerProductPrice({ customerId: customerId }))
  }

  interface ProductInfo {
    uid: string
    name: string
    category: SimpleCategoryData
    brand: SimpleBrandData
    primaryUnit: SimpleUnitData
    enabled: boolean
    description?: string
    disabled: boolean
  }

  interface ProductUnitInfo {
    uid: string
    name: string
    enabled: boolean
    isPrimary: boolean
  }

  const rules = reactive({
    groupId: [
      {
        required: true,
        message: '请选择价格组',
      },
    ],
  })

  const priceColumns = [
    {
      title: '商品',
      dataIndex: 'productName',
      key: 'productName',
      slots: { customRender: 'productId' },
      width: '20%',
    },
    {
      title: '品牌',
      dataIndex: 'brandName',
      key: 'brandName',
      width: '20%',
    },
    {
      title: '单位',
      dataIndex: 'unitName',
      key: 'unitName',
      slots: { customRender: 'unitId' },
      width: '20%',
    },
    {
      title: '价格',
      dataIndex: 'price',
      key: 'price',
      slots: { customRender: 'price' },
      width: '20%',
    },
    {
      title: '操作',
      key: 'action',
      slots: { customRender: 'action' },
    },
  ]

  const editingKey = ref('')
  const fetching = ref(false)

  function addNewLine() {}

  function edit(key: string) {
    if (!editingKey.value) {
      editingKey.value = key
    } else {
      message.info('请先保存正在编辑的数据')
    }
  }

  const checkGroupSelected = computed(() => !!customerIdRef.value)

  return {
    ...toRefs(customerPriceParamRef),
    rules,
    priceColumns,
    addNewLine,
    editingKey,
    edit,
    fetching,
    checkGroupSelected,
    customerSelected,
    customerIdRef,
  }
}
