import { useHttpRequest } from '@/api/chunfeng/apiRequest'
import { UnwrapNestedRefs } from '@vue/reactivity'

import { reactive, ref } from 'vue'
import { useExpenseTypeApi } from '@/api/chunfeng/finance/expense-type'
import {
  ExpenseTypeData,
  QueryExpenseTypeParam,
} from '@/api/chunfeng/finance/expense-type/interfaces'

export const useExpenseTypeListFunctions = () => {
  // name: 'ReceiptList',
  const { executeQueryRequest } = useHttpRequest()
  const { queryExpenseType } = useExpenseTypeApi()

  const resultDataSet = ref(Array<ExpenseTypeData>())

  const paginationConfig = reactive({
    showSizeChanger: true,
  })

  const queryParams: UnwrapNestedRefs<QueryExpenseTypeParam> = reactive({
    name: undefined,
    enabled: undefined,
  })

  const columns = [
    {
      title: '类型名称',
      dataIndex: 'name',
      key: 'name',
    },
    {
      title: '状态',
      dataIndex: 'enabled',
      key: 'enabled',
      slots: { customRender: 'enabled' },
    },
    {
      title: '备注',
      dataIndex: 'remark',
      key: 'remark',
    },
    {
      title: '创建日期',
      dataIndex: 'createdAt',
      key: 'createdAt',
    },

    {
      title: '操作',
      key: 'action',
      slots: { customRender: 'action' },
    },
  ]

  function reset() {
    queryParams.enabled = undefined
    queryParams.name = undefined
  }

  function query() {
    resultDataSet.value.length = 0
    executeQueryRequest(queryExpenseType(queryParams), (results: ExpenseTypeData[]) => {
      resultDataSet.value.push(...results)
    })
  }

  return {
    paginationConfig,
    queryParams,
    columns,
    reset,
    query,
    resultDataSet,
  }
}
