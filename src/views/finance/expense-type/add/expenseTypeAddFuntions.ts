import { reactive, ref } from 'vue'
import { useForm } from 'ant-design-vue/es/form'
import { useHttpRequest } from '@/api/chunfeng/apiRequest'
import { useExpenseTypeApi } from '@/api/chunfeng/finance/expense-type'
import { NewExpenseTypeParam } from '@/api/chunfeng/finance/expense-type/interfaces'

export const useExpenseTypeAddFunctions = () => {
  const newParam = reactive<NewExpenseTypeParam>({
    name: '',
    remark: undefined,
  })
  const rules = reactive({
    name: [
      {
        required: true,
        message: '请输入类型名称',
      },
    ],
    remark: [
      {
        required: false,
      },
    ],
  })

  const { validateInfos, resetFields, validate } = useForm(newParam, rules)
  const { executeCommandRequest } = useHttpRequest()
  const { newExpenseType } = useExpenseTypeApi()

  const layout = {
    labelCol: { span: 4 },
    wrapperCol: { span: 14 },
  }

  function addExpenseType() {
    validate().then((data: NewExpenseTypeParam) => {
      executeCommandRequest(newExpenseType(data), {
        message: '新增费用类型成功',
        callback: () => {
          resetFields()
        },
      })
    })
  }

  function reset() {
    resetFields()
  }

  return {
    newParam,
    validateInfos,
    addExpenseType,
    reset,
    layout,
  }
}
