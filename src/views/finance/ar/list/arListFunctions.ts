import { useHttpRequest } from '@/api/chunfeng/apiRequest'
import { UnwrapNestedRefs } from '@vue/reactivity'

import { reactive, ref } from 'vue'

import { Form } from 'ant-design-vue'
import moment from 'moment'
import { useArApi } from '@/api/chunfeng/finance/ar'
import { ArData, QueryArParam } from '@/api/chunfeng/finance/ar/interfaces'

export const useReceiptListFunctions = () => {
  // name: 'ReceiptList',
  const { executeQueryRequest, executeCommandRequest } = useHttpRequest()
  const { queryAr } = useArApi()

  const resultDataSet = ref(Array<ArData>())
  const csRef = ref()

  const paginationConfig = reactive({
    showSizeChanger: true,
  })

  const queryParams: UnwrapNestedRefs<QueryArParam> = reactive({
    customerId: undefined,
    status: undefined,
  })

  const columns = [
    {
      title: '客户名称',
      dataIndex: 'customerName',
      key: 'customerName',
    },

    {
      title: '金额',
      dataIndex: 'amount',
      key: 'amount',
      slots: { customRender: 'amount' },
    },
    {
      title: '状态',
      dataIndex: 'status',
      key: 'status',
      slots: { customRender: 'status' },
    },
    {
      title: '发票ID',
      dataIndex: 'sourceId',
      key: 'sourceId',
      slots: { customRender: 'sourceId' },
    },
    {
      title: '发生日期',
      dataIndex: 'occurredAt',
      key: 'occurredAt',
    },
    {
      title: '未核销金额',
      dataIndex: 'unConfirmed',
      key: 'unConfirmed',
    },
    {
      title: '创建时间',
      dataIndex: 'createdAt',
      key: 'createdAt',
    },
    {
      title: '操作',
      key: 'action',
      slots: { customRender: 'action' },
    },
  ]

  function reset() {
    csRef.value.reset()
    queryParams.customerId = undefined
    queryParams.status = undefined
  }

  function query() {
    resultDataSet.value.length = 0

    executeQueryRequest(queryAr(queryParams), (results: ArData[]) => {
      resultDataSet.value.push(...results)
    })
  }

  function customerSelected(customerId: string) {
    queryParams.customerId = customerId
  }

  return {
    paginationConfig,
    queryParams,
    columns,
    reset,
    query,
    resultDataSet,
    customerSelected,
    csRef,
  }
}
