/**
 * 用户信息模块
 */
import { IUserState, state } from './state'
import { mutations } from './mutations'
import { actions } from './actions'
import { getters } from './getters'
import { App, InjectionKey } from 'vue'
import { createStore, Store, useStore as baseUseStore } from 'vuex'

const key: InjectionKey<Store<IUserState>> = Symbol()

const store = createStore<IUserState>({
  state,
  mutations,
  actions,
  getters
})

export function useUserStore() {
  return baseUseStore(key) || store
}

export function install(app: App) {
  app.use(store, key)
}
