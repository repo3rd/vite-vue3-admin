export interface AuthState {
  token?: string
  expired?: Date
}
