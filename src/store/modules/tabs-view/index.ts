/**
 * 标签页
 */
import { ITabsViewState, state } from './state'
import { mutations } from './mutations'
import { App, InjectionKey } from 'vue'
import { createStore, Store } from 'vuex'
import { useStore as baseUseStore } from 'vuex'

export type { ITabsViewState } from './state'

const key: InjectionKey<Store<ITabsViewState>> = Symbol()
const store = createStore<ITabsViewState>({
  state,
  mutations
})

export function useTabsViewStore() {
  return baseUseStore(key)
}

export function install(app: App) {
  app.use(store, key)
}
