/**
 * 向后端请求用户的菜单，动态生成路由
 */

import { IAsyncRouteState, state } from './state'
import { getters } from './getters'
import { mutations } from './mutations'
import { actions } from './actions'
import { createStore, Store, useStore as baseUseStore } from 'vuex'
import { App, InjectionKey } from 'vue'

export type { IAsyncRouteState } from './state'
const key: InjectionKey<Store<IAsyncRouteState>> = Symbol()
const store = createStore<IAsyncRouteState>({
  state,
  getters,
  mutations,
  actions,
})

export function useAsyncRouteStore(): Store<IAsyncRouteState> {
  return baseUseStore(key)
}
export function useAsyncRouteStoreOut(): Store<IAsyncRouteState> {
  return store
}

export function install(app: App) {
  app.use(store, key)
}
