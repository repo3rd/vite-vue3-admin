/**
 * 锁屏
 */

import { ILockscreenState, state } from './state'
import { mutations } from './mutations'
import { App, InjectionKey } from 'vue'
import { createStore, Store, useStore as baseUseStore } from 'vuex'

export type { ILockscreenState } from './state'

const key: InjectionKey<Store<ILockscreenState>> = Symbol()
const store = createStore<ILockscreenState>({
  state,
  mutations
})

export function useLockscreenStore(): Store<ILockscreenState> {
  return baseUseStore(key)
}

export function install(app: App) {
  app.use(store, key)
}
