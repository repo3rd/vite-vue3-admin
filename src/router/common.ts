import dashboard from '@/router/modules/dashboard'
import demos from '@/router/modules/demos'
import redirect from '@/router/modules/redirect'
import customer from '@/router/modules/customer'
import brand from '@/router/modules/brand'
import category from '@/router/modules/category'
import product from '@/router/modules/product'
import unit from '@/router/modules/unit'
import sale from '@/router/modules/sale'
import finance from '@/router/modules/finance'
import wms from '@/router/modules/wms'

export default [
  ...dashboard,
  ...demos,
  ...redirect,
  ...customer,
  ...brand,
  ...category,
  ...product,
  // ...accounting,
  // ...accountingSubject,
  ...unit,
  ...sale,
  ...finance,
  ...wms,
]
