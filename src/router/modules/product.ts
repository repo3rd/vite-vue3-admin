import { RouteRecordRaw } from 'vue-router'
import { RouterTransition } from '@/components/transition'

const routeName = 'product'

const routes: Array<RouteRecordRaw> = [
  {
    path: '/product',
    name: routeName,
    redirect: '/product/list',
    component: RouterTransition,
    meta: {
      title: '商品管理',
      icon: 'icon-zhuomian',
    },
    children: [
      {
        path: 'list',
        name: `${routeName}-list`,
        meta: {
          title: '商品列表',
          icon: 'icon-list',
        },
        component: () => import('@/views/product/list/index.vue'),
      },
      {
        path: 'add',
        name: `${routeName}-add`,
        meta: {
          title: '新增商品',
          icon: 'icon-add',
        },
        component: () => import('@/views/product/add/index.vue'),
      },
      {
        path: 'edit',
        name: `${routeName}-edit`,
        meta: {
          title: '编辑商品',
          icon: 'icon-edit',
          hidden: true,
        },
        component: () => import('@/views/product/edit/index.vue'),
      },
    ],
  },
]

export default routes
