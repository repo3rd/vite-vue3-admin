import { RouteRecordRaw } from 'vue-router'
import { RouterTransition } from '@/components/transition'

const routeName = 'category'

const routes: Array<RouteRecordRaw> = [
  {
    path: '/category',
    name: routeName,
    redirect: '/category/list',
    component: RouterTransition,
    meta: {
      title: '分类管理',
      icon: 'icon-zhuomian',
    },
    children: [
      {
        path: 'list',
        name: `${routeName}-list`,
        meta: {
          title: '分类列表',
          icon: 'icon-list',
        },
        component: () => import('@/views/category/list/index.vue'),
      },
      {
        path: 'add',
        name: `${routeName}-add`,
        meta: {
          title: '新增分类',
          icon: 'icon-add',
        },
        component: () => import('@/views/category/add/index.vue'),
      },
    ],
  },
]

export default routes
