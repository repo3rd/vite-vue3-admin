import { RouteRecordRaw } from 'vue-router'
import { RouterTransition } from '@/components/transition'

const routeName = 'finance'
const routeName_receipt = 'receipt'
const routeName_expense = 'expense'
const routeName_expense_type = 'expense-type'
const routeName_ar = 'expense-ar'

const routes: Array<RouteRecordRaw> = [
  {
    path: '/finance',
    name: routeName,
    redirect: '/finance/receipt/list',
    component: RouterTransition,
    meta: {
      title: '财务管理',
      icon: 'icon-zhuomian',
    },
    children: [
      {
        path: 'receipt',
        name: `${routeName_receipt}`,
        meta: {
          title: '收款管理',
          icon: 'icon-shoukuan',
        },
        redirect: '/finance/receipt/list',
        component: RouterTransition,
        children: [
          {
            path: 'add',
            name: `${routeName_receipt}-add`,
            meta: {
              title: '新增收款',
              icon: 'icon-add',
            },
            component: () => import('@/views/finance/receipt/add/index.vue'),
          },
          {
            path: 'list',
            name: `${routeName_receipt}-list`,
            meta: {
              title: '收款查询',
              icon: 'icon-sousuo',
            },
            component: () => import('@/views/finance/receipt/list/index.vue'),
          },
        ],
      },
      {
        path: 'ar',
        name: `${routeName_ar}`,
        meta: {
          title: '应收管理',
          icon: 'icon-shoukuan',
        },
        redirect: '/finance/ar/list',
        component: RouterTransition,
        children: [
          {
            path: 'list',
            name: `${routeName_ar}-list`,
            meta: {
              title: '应收查询',
              icon: 'icon-sousuo',
            },
            component: () => import('@/views/finance/ar/list/index.vue'),
          },
        ],
      },

      {
        path: 'expense',
        name: `${routeName_expense}`,
        meta: {
          title: '费用管理',
          icon: 'icon-shoukuan',
        },
        redirect: '/finance/expense/list',
        component: RouterTransition,
        children: [
          {
            path: 'add',
            name: `${routeName_expense}-add`,
            meta: {
              title: '新增费用',
              icon: 'icon-add',
            },
            component: () => import('@/views/finance/expense/add/index.vue'),
          },
          {
            path: 'list',
            name: `${routeName_expense}-list`,
            meta: {
              title: '费用查询',
              icon: 'icon-sousuo',
            },
            component: () => import('@/views/finance/expense/list/index.vue'),
          },
        ],
      },
      {
        path: 'expenseType',
        name: `${routeName_expense_type}`,
        meta: {
          title: '费用类型管理',
          icon: 'icon-shoukuan',
        },
        redirect: '/finance/expenseType/list',
        component: RouterTransition,
        children: [
          {
            path: 'add',
            name: `${routeName_expense_type}-add`,
            meta: {
              title: '新增费用类型',
              icon: 'icon-add',
            },
            component: () => import('@/views/finance/expense-type/add/index.vue'),
          },
          {
            path: 'list',
            name: `${routeName_expense_type}-list`,
            meta: {
              title: '费用类型查询',
              icon: 'icon-sousuo',
            },
            component: () => import('@/views/finance/expense-type/list/index.vue'),
          },
        ],
      },
    ],
  },
]

export default routes
