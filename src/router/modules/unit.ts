import { RouteRecordRaw } from 'vue-router'
import { RouterTransition } from '@/components/transition'

const routeName = 'unit'

const routes: Array<RouteRecordRaw> = [
  {
    path: '/unit',
    name: routeName,
    redirect: '/unit/list',
    component: RouterTransition,
    meta: {
      title: '单位管理',
      icon: 'icon-zhuomian',
    },
    children: [
      {
        path: 'list',
        name: `${routeName}-list`,
        meta: {
          title: '单位列表',
          icon: 'icon-list',
        },
        component: () => import('@/views/unit/list/index.vue'),
      },
      {
        path: 'add',
        name: `${routeName}-add`,
        meta: {
          title: '新增单位',
          icon: 'icon-add',
        },
        component: () => import('@/views/unit/add/index.vue'),
      },
    ],
  },
]

export default routes
