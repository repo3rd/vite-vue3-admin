import { RouteRecordRaw } from 'vue-router'
import { RouterTransition } from '@/components/transition'

const routeName = 'sale'
const priceRouteName = 'sale-price'
const orderRouteName = 'sale-order'
const priceGroupRouteName = 'sale-pricegroup'
const customerProductPriceRouteName = 'sale-cpp'

const routes: Array<RouteRecordRaw> = [
  {
    path: '/sale',
    name: routeName,
    redirect: '/sale/order/list',
    component: RouterTransition,
    meta: {
      title: '销售管理',
      icon: 'icon-zhuomian',
    },
    children: [
      {
        path: 'order',
        name: `${orderRouteName}`,
        redirect: '/sale/order/list',
        meta: {
          title: '订单管理',
          icon: 'icon-zhuomian',
        },
        component: RouterTransition,
        children: [
          {
            path: 'list',
            name: `${orderRouteName}-list`,
            meta: {
              title: '订单列表',
              icon: 'icon-list',
            },
            component: () => import('@/views/sale/order/list/index.vue'),
          },
          {
            path: 'add',
            name: `${orderRouteName}-add`,
            meta: {
              title: '新增订单',
              icon: 'icon-bianji',
            },
            component: () => import('@/views/sale/order/add/index.vue'),
          },
        ],
      },
      {
        path: 'price',
        name: `${priceRouteName}`,
        redirect: '/sale/price/list',
        meta: {
          title: '价格管理',
          icon: 'icon-zhuomian',
        },
        component: RouterTransition,
        children: [
          {
            path: 'add',
            name: `${priceRouteName}-add`,
            meta: {
              title: '新增价格',
              icon: 'icon-bianji',
            },
            component: () => import('@/views/sale/price/add/index.vue'),
          },
        ],
      },
      {
        path: 'pricegroup',
        name: `${priceGroupRouteName}`,
        redirect: '/sale/pricegroup/list',
        meta: {
          title: '价格组管理',
          icon: 'icon-zhuomian',
        },
        component: RouterTransition,
        children: [
          {
            path: 'add',
            name: `${priceGroupRouteName}-add`,
            meta: {
              title: '新增价格组',
              icon: 'icon-bianji',
            },
            component: () => import('@/views/sale/price-group/add/index.vue'),
          },
          {
            path: 'list',
            name: `${priceGroupRouteName}-list`,
            meta: {
              title: '价格组列表',
              icon: 'icon-sousuo',
            },
            component: () => import('@/views/sale/price-group/list/index.vue'),
          },
        ],
      },

      {
        path: 'cpp',
        name: `${customerProductPriceRouteName}`,
        redirect: '/sale/cpp/list',
        meta: {
          title: '客户商品价格管理',
          icon: 'icon-zhuomian',
        },
        component: RouterTransition,
        children: [
          {
            path: 'add',
            name: `${customerProductPriceRouteName}-add`,
            meta: {
              title: '设置客户商品价格',
              icon: 'icon-bianji',
            },
            component: () => import('@/views/sale/customer-price/add/index.vue'),
          },
          {
            path: 'list',
            name: `${customerProductPriceRouteName}-list`,
            meta: {
              title: '客户商品价格列表',
              icon: 'icon-sousuo',
            },
            component: () => import('@/views/sale/customer-price/list/index.vue'),
          },
        ],
      },
    ],
  },
]

export default routes
