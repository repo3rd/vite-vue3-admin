import { RouteRecordRaw } from 'vue-router'
import { RouterTransition } from '@/components/transition'

const routeName = 'wms'
const routeName_warehouse = 'wms-warehouse'
const routeName_do = 'wms-do'

const routes: Array<RouteRecordRaw> = [
  {
    path: '/wms',
    name: routeName,
    redirect: '/wms/wh/list',
    component: RouterTransition,
    meta: {
      title: '仓储管理',
      icon: 'icon-zhuomian',
    },
    children: [
      {
        path: 'wh',
        name: `${routeName_warehouse}`,
        meta: {
          title: '仓库管理',
          icon: 'icon-shoukuan',
        },
        redirect: '/wms/wh/list',
        component: RouterTransition,
        children: [
          {
            path: 'add',
            name: `${routeName_warehouse}-add`,
            meta: {
              title: '新增仓库',
              icon: 'icon-add',
            },
            component: () => import('@/views/wms/warehouse/add/index.vue'),
          },
          {
            path: 'list',
            name: `${routeName_warehouse}-list`,
            meta: {
              title: '仓库查询',
              icon: 'icon-sousuo',
            },
            component: () => import('@/views/wms/warehouse/list/index.vue'),
          },
        ],
      },

      {
        path: 'do',
        name: `${routeName_do}`,
        meta: {
          title: '出库单管理',
          icon: 'icon-shoukuan',
        },
        redirect: '/wms/wh/list',
        component: RouterTransition,
        children: [
          {
            path: 'add',
            name: `${routeName_do}-add`,
            meta: {
              title: '新增出库单',
              icon: 'icon-add',
            },
            component: () => import('@/views/wms/delivery/add/index.vue'),
          },
          {
            path: 'list',
            name: `${routeName_do}-list`,
            meta: {
              title: '出库单查询',
              icon: 'icon-sousuo',
            },
            component: () => import('@/views/wms/delivery/list/index.vue'),
          },
        ],
      },
    ],
  },
]

export default routes
