import { RouteRecordRaw } from 'vue-router'
import { RouterTransition } from '@/components/transition'

const routeName = 'accountingSubject'

const routes: Array<RouteRecordRaw> = [
  {
    path: '/accsub',
    name: routeName,
    redirect: '/accsub/list',
    component: RouterTransition,
    meta: {
      title: '科目管理',
      icon: 'icon-zhuomian',
    },
    children: [
      {
        path: 'list',
        name: `${routeName}-list`,
        meta: {
          title: '科目列表',
          icon: 'icon-list',
        },
        component: () => import('@/views/accounting-subject/list/index.vue'),
      },
      {
        path: 'add',
        name: `${routeName}-add`,
        meta: {
          title: '新增科目',
          icon: 'icon-add',
        },
        component: () => import('@/views/accounting-subject/add/index.vue'),
      },
    ],
  },
]

export default routes
