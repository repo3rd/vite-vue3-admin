import { RouteRecordRaw } from 'vue-router'
import { RouterTransition } from '@/components/transition'

const routeName = 'customer'

const routes: Array<RouteRecordRaw> = [
  {
    path: '/customer',
    name: routeName,
    redirect: '/customer/list',
    component: RouterTransition,
    meta: {
      title: '客户管理',
      icon: 'icon-zhuomian',
    },
    children: [
      {
        path: 'list',
        name: `${routeName}-list`,
        meta: {
          title: '客户列表',
          icon: 'icon-list',
        },
        component: () => import('@/views/customer/list/index.vue'),
      },
      {
        path: 'add',
        name: `${routeName}-add`,
        meta: {
          title: '新增客户',
          icon: 'icon-add',
        },
        component: () => import('@/views/customer/add/index.vue'),
      },
    ],
  },
]

export default routes
