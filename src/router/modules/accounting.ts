import { RouteRecordRaw } from 'vue-router'
import { RouterTransition } from '@/components/transition'

const routeName = 'accounting'

const routes: Array<RouteRecordRaw> = [
  {
    path: '/accounting',
    name: routeName,
    redirect: '/accounting/list',
    component: RouterTransition,
    meta: {
      title: '记账管理',
      icon: 'icon-zhuomian',
    },
    children: [
      {
        path: 'list',
        name: `${routeName}-list`,
        meta: {
          title: '记账列表',
          icon: 'icon-list',
        },
        component: () => import('@/views/accounting/list/index.vue'),
      },
      {
        path: 'add',
        name: `${routeName}-add`,
        meta: {
          title: '新增记账',
          icon: 'icon-add',
        },
        component: () => import('@/views/accounting/add/index.vue'),
      },
    ],
  },
]

export default routes
