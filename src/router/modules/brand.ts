import { RouteRecordRaw } from 'vue-router'
import { RouterTransition } from '@/components/transition'

const routeName = 'brand'

const routes: Array<RouteRecordRaw> = [
  {
    path: '/brand',
    name: routeName,
    redirect: '/brand/list',
    component: RouterTransition,
    meta: {
      title: '品牌管理',
      icon: 'icon-zhuomian',
    },
    children: [
      {
        path: 'list',
        name: `${routeName}-list`,
        meta: {
          title: '品牌列表',
          icon: 'icon-list',
        },
        component: () => import('@/views/brand/list/index.vue'),
      },
      {
        path: 'add',
        name: `${routeName}-add`,
        meta: {
          title: '新增品牌',
          icon: 'icon-add',
        },
        component: () => import('@/views/brand/add/index.vue'),
      },
    ],
  },
]

export default routes
