import moment from 'moment'

export interface NewWarehouseParam {
  name: string
  address: string
  remark?: string
}

export interface QueryWarehouseParam {
  name?: string
  enabled?: boolean
}

export interface WarehouseData {
  uid: string
  name: string
  address: string
  remark?: string
  enabled: boolean
  createdAt: moment.Moment
}
