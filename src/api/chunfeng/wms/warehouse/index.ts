import { NewWarehouseParam, QueryWarehouseParam } from '@/api/chunfeng/wms/warehouse/interfaces'
import { apiRequest } from '@/api/chunfeng/apiRequest'

enum WmsApiPaths {
  new = '/wms/wh/new',
  query = '/wms/wh/query',
  enable = '/wms/wh/enable',
  disable = '/wms/wh/disable',
}

export const useWarehouseApi = () => {
  function newWarehouse(data: NewWarehouseParam) {
    return apiRequest(WmsApiPaths.new, 'post', data)
  }

  function queryWarehouse(data: QueryWarehouseParam) {
    return apiRequest(WmsApiPaths.query, 'get', data)
  }

  function enableWarehouse(uid: string) {
    return apiRequest(WmsApiPaths.enable + '/' + uid, 'post')
  }

  function disableWarehouse(uid: string) {
    return apiRequest(WmsApiPaths.disable + '/' + uid, 'post')
  }

  return {
    newWarehouse,
    queryWarehouse,
    enableWarehouse,
    disableWarehouse,
  }
}
