import moment from 'moment'

export interface QueryDeliveryOrderParam {
  warehouseId: string
  customerId?: string
  sourceId?: string
  status?: string
  occurredStart?: moment.Moment
  occurredEnd?: moment.Moment
}

export interface FinishDeliveryOrderParam {
  uid: string
  items: FinishDeliveryOrderItemParam[]
}

export interface FinishDeliveryOrderItemParam {
  uid: string
  productId: string
  done: number
}

export interface DeliveryOrderData {
  uid: string
  warehouseId: string
  customerId: string
  customerName: string
  status: string
  items: DeliveryOrderItemData[]
  occurredAt: moment.Moment
  createdAt: moment.Moment
}

interface DeliveryOrderItemData {
  productId: string
  productName: string
  demand: number
  done: number
}
