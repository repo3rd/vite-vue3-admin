import {
  FinishDeliveryOrderParam,
  QueryDeliveryOrderParam,
} from '@/api/chunfeng/wms/delivery/interfaces'
import { apiRequest } from '@/api/chunfeng/apiRequest'

enum DeliveryOrderApiPaths {
  new = '/wms/do/new',
  query = '/wms/do/query',
  finish = '/wms/do/finish',
}

export const useDeliveryOrderApi = () => {
  function queryDeliveryOrder(data: QueryDeliveryOrderParam) {
    return apiRequest(DeliveryOrderApiPaths.query, 'get', data)
  }

  function finishDeliveryOrder(data: FinishDeliveryOrderParam) {
    return apiRequest(DeliveryOrderApiPaths.finish, 'post', data)
  }

  return {
    queryDeliveryOrder,
    finishDeliveryOrder,
  }
}
