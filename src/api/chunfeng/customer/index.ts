import {
  AddCustomerData,
  CustomerData,
  QueryCustomerData,
  StartBusinessData,
} from '@/api/chunfeng/customer/interfaces'
import { apiRequest } from '@/api/chunfeng/apiRequest'

enum CustomerApis {
  query = '/customer/query',
  add = '/customer/add',
  enable = '/customer/enable',
  disable = '/customer/disable',
  startBusiness = '/customer/business/start',
  stopBusiness = '/customer/business/stop',
}

export const useCustomerApi = () => {
  async function queryCustomer(params: QueryCustomerData) {
    return apiRequest<CustomerData[]>(CustomerApis.query, 'GET', params)
  }

  function disableCustomer(uid: string) {
    return apiRequest(CustomerApis.disable + `/${uid}`, 'POST')
  }

  function enableCustomer(uid: string) {
    return apiRequest(CustomerApis.enable + `/${uid}`, 'POST')
  }

  function addCustomer(data: AddCustomerData) {
    return apiRequest(CustomerApis.add, 'POST', data)
  }

  function startBusiness(data: StartBusinessData) {
    return apiRequest(CustomerApis.startBusiness, 'POST', data)
  }

  function stopBusiness(uid: string) {
    return apiRequest(CustomerApis.stopBusiness, 'POST', { uid })
  }

  return {
    queryCustomer,
    disableCustomer,
    enableCustomer,
    addCustomer,
    stopBusiness,
    startBusiness,
  }
}
