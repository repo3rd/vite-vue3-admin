import moment from 'moment'

export interface QueryCustomerData {
  name?: string
  contact?: string
  mobile?: string
  address?: string
  enabled?: boolean
}

export interface CustomerData {
  uid: string
  name: string
  contact: string
  mobile?: string
  address?: string
  enabled: boolean
  remark?: string
  businessStatus?: CustomerBusinessStatus
  createdAt: Date
}

export interface CustomerBusinessStatus {
  uid: string
  enabled: boolean
  startedAt?: moment.Moment
  endedAt?: moment.Moment
}

export interface AddCustomerData {
  name: string
  contact: string
  mobile?: string
  address?: string
  remark?: string
  startedAt: moment.Moment
}

export interface StartBusinessData {
  uid: string
  startedAt: moment.Moment
}

export interface SimpleCustomerData {
  uid: string
  name: string
}
