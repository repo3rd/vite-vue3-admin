import moment from 'moment'
import { SimpleCategoryData } from '@/api/chunfeng/category/interfaces'
import { SimpleBrandData } from '@/api/chunfeng/brand/interfaces'
import { SimpleUnitData, UnitData } from '@/api/chunfeng/unit/interfaces'

export interface AddProductData {
  name: string
  categoryId: string
  brandId: string
  unitId: string
  description?: string
}

export interface ProductParam {
  uid: string
  name: string
  categoryId: string
  brandId: string
  unitId: string
  description?: string
}

export interface AddProductUnitData {
  uid: string
  unitId: string
  primary: boolean
}

export interface RemoveProductUnitParam {
  uid: string
  productUnitId: string
}

export interface QueryProductData {
  name?: string
  categoryId?: string
  brandId?: string
  enabled?: boolean
}

export interface ProductData {
  uid: string
  name: string
  category: SimpleCategoryData
  brand: SimpleBrandData
  primaryUnit: SimpleUnitData
  enabled: boolean
  description?: string
  createdAt: moment.Moment
}

export interface ProductUnitData {
  uid: string
  productId: string
  unitId: string
  isPrimary: boolean
  unit: UnitData
}
