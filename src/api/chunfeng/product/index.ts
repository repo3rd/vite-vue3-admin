import {
  AddProductData,
  AddProductUnitData,
  ProductUnitData,
  QueryProductData,
  RemoveProductUnitParam,
  ProductParam,
} from '@/api/chunfeng/product/interfaces'
import { apiRequest } from '@/api/chunfeng/apiRequest'
import product from '@/router/modules/product'

enum ProductApiPaths {
  add = '/product/add',
  enable = '/product/enable',
  disable = '/product/disable',
  query = '/product/query',
  queryUnit = '/product/unit/query',
  getById = '/product/id',
  addUnit = '/product/unit/add',
  removeUnit = '/product/unit/remove',
  update = '/product/update',
}

export const useProductApi = () => {
  function addProduct(data: AddProductData) {
    return apiRequest(ProductApiPaths.add, 'post', data)
  }

  function updateProduct(data: ProductParam) {
    return apiRequest(ProductApiPaths.update, 'post', data)
  }

  function getProductById(uid: string) {
    return apiRequest(ProductApiPaths.getById + '/' + uid, 'get', {})
  }

  function queryProduct(data: QueryProductData) {
    return apiRequest(ProductApiPaths.query, 'get', data)
  }

  function enableProduct(uid: string) {
    return apiRequest(ProductApiPaths.enable + '/' + uid, 'post')
  }

  function disableProduct(uid: string) {
    return apiRequest(ProductApiPaths.disable + '/' + uid, 'post')
  }

  function queryProductUnit(productId: string) {
    return apiRequest<ProductUnitData>(ProductApiPaths.queryUnit, 'get', {
      productId: productId,
      enabled: true,
    })
  }

  function addProductUnit(data: AddProductUnitData[]) {
    return apiRequest(ProductApiPaths.addUnit, 'post', data)
  }

  function removeProductUnit(data: RemoveProductUnitParam) {
    return apiRequest(ProductApiPaths.removeUnit, 'post', data)
  }

  return {
    addProduct,
    queryProduct,
    enableProduct,
    disableProduct,
    queryProductUnit,
    getProductById,
    addProductUnit,
    removeProductUnit,
    updateProduct,
  }
}
