import { apiRequest } from '@/api/chunfeng/apiRequest'
import { UserLoginParam } from '@/api/chunfeng/auth/interfaces'

const AuthApiPaths = {
  login: '/auth/login',
}

export function login(params: UserLoginParam): Promise<TokenResult> {
  return apiRequest<TokenResult>(AuthApiPaths.login, 'POST', params)
}

export interface TokenResult {
  uid: string
  value: string
  expired: string
}
