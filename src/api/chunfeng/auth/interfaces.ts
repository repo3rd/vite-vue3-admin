export interface UserLoginParam {
  userName: string
  password: string
}
