import {
  AddAccountingSubjectData,
  AddJournalEntryData,
  QueryAccountingSubjectData,
  QueryJournalEntryData,
} from '@/api/chunfeng/accounting/interfaces'
import { apiRequest } from '@/api/chunfeng/apiRequest'

enum AccountingApiPaths {
  as_add = '/accsub/add',
  as_enable = '/accsub/enable',
  as_disable = '/accsub/disable',
  as_query = '/accsub/query',
  add = '/accounting/add',
  query = '/accounting/query',
}

export const useAccountingApi = () => {
  function addAccountingSubject(data: AddAccountingSubjectData) {
    return apiRequest(AccountingApiPaths.as_add, 'post', data)
  }

  function enableAccountingSubject(uid: string) {
    return apiRequest(AccountingApiPaths.as_enable + `/${uid}`, 'post', {})
  }

  function disableAccountingSubject(uid: string) {
    return apiRequest(AccountingApiPaths.as_disable + `/${uid}`, 'post', {})
  }

  function queryAccountingSubject(query: QueryAccountingSubjectData) {
    return apiRequest(AccountingApiPaths.as_query, 'get', query)
  }

  function addJournalEntry(data: AddJournalEntryData) {
    return apiRequest(AccountingApiPaths.add, 'post', data)
  }

  function queryJournalEntry(data: QueryJournalEntryData) {
    return apiRequest(AccountingApiPaths.query, 'get', data)
  }

  return {
    addAccountingSubject,
    enableAccountingSubject,
    disableAccountingSubject,
    addJournalEntry,
    queryAccountingSubject,
    queryJournalEntry,
  }
}
