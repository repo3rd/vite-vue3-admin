import moment from 'moment'

export interface AccountingSubjectData {
  uid: string
  name: string
  type: string
  createdAt: moment.Moment
}

export interface AddAccountingSubjectData {
  name: string
  type: string
}

export interface QueryAccountingSubjectData {
  name?: string
  type?: string
  enabled?: boolean
}

export interface JournalEntryData {
  uid: string
  name: string
  accountingSubject: AccountingSubjectData
  amount: number
  occurredAt: moment.Moment
  createdAt: moment.Moment
}

export interface AddJournalEntryData {
  name: string
  accountingSubjectId: string
  amount: number
  occurredAt: moment.Moment
}

export interface QueryJournalEntryData {
  name?: string
  type?: 'income' | 'expense'
  accountingSubjectId?: string
  occurredStart?: moment.Moment
  occurredEnd?: moment.Moment
}

export type AccountingType = 'income' | 'expense'
