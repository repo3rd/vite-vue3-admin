import { AddUnitData, QueryUnitData } from '@/api/chunfeng/unit/interfaces'
import { apiRequest } from '@/api/chunfeng/apiRequest'

enum UnitApiPaths {
  add = '/unit/add',
  disable = '/unit/disable',
  enable = '/unit/enable',
  query = '/unit/query',
}

export const useUnitApi = () => {
  function addUnit(data: AddUnitData) {
    return apiRequest(UnitApiPaths.add, 'post', data)
  }

  function disableUnit(uid: string) {
    return apiRequest(UnitApiPaths.disable + `/${uid}`, 'post', {})
  }

  function enableUnit(uid: string) {
    return apiRequest(UnitApiPaths.enable + `/${uid}`, 'post', {})
  }

  function queryUnit(data: QueryUnitData) {
    return apiRequest(UnitApiPaths.query, 'get', data)
  }

  return {
    addUnit,
    disableUnit,
    enableUnit,
    queryUnit,
  }
}
