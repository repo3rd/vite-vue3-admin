import moment from 'moment'

export interface UnitData {
  uid: string
  name: string
  enabled: boolean
  createdAt: moment.Moment
}

export interface AddUnitData {
  name: string
}

export interface QueryUnitData {
  name?: string
  enabled?: boolean
}

export interface SimpleUnitData {
  uid: string
  name: string
  enabled: boolean
}
