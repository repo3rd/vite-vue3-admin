import moment from 'moment'

export interface AddBrandData {
  name: string
  remark?: string
}

export interface QueryBrandData {
  name?: string
  enabled?: boolean
}

export interface BrandData {
  uid: string
  name: string
  remark?: string
  enabled: boolean
  createdAt: moment.Moment
}

export interface SimpleBrandData {
  uid: string
  name: string
}
