import { AddBrandData, QueryBrandData } from '@/api/chunfeng/brand/interfaces'
import { apiRequest } from '@/api/chunfeng/apiRequest'

enum BrandApiPaths {
  add = '/brand/add',
  enable = '/brand/enable',
  disable = '/brand/disable',
  query = '/brand/query',
}

export const useBrandApi = () => {
  function addBrand(addBrandData: AddBrandData) {
    return apiRequest(BrandApiPaths.add, 'post', addBrandData)
  }

  function enableBrand(uid: string) {
    return apiRequest(BrandApiPaths.enable, 'post', { uid })
  }

  function disableBrand(uid: string) {
    return apiRequest(BrandApiPaths.disable, 'post', { uid })
  }

  function queryBrand(queryBrandData: QueryBrandData) {
    return apiRequest(BrandApiPaths.query, 'get', queryBrandData)
  }

  return {
    addBrand,
    enableBrand,
    disableBrand,
    queryBrand,
  }
}
