import { AddCategoryData, QueryCategoryData } from '@/api/chunfeng/category/interfaces'
import { apiRequest } from '@/api/chunfeng/apiRequest'

enum CategoryApiPaths {
  add = '/category/add',
  queryFirstLevelEnabled = '/category/list/enabled',
  query = '/category/query',
  enable = '/category/enable',
  disable = '/category/disable',
}

export const useCategoryApi = () => {
  function addCategory(addCategoryData: AddCategoryData) {
    return apiRequest(CategoryApiPaths.add, 'post', addCategoryData)
  }

  function queryFirstLevelEnabledCategory() {
    return apiRequest(CategoryApiPaths.queryFirstLevelEnabled, 'get')
  }

  function queryCategory(data: QueryCategoryData) {
    return apiRequest(CategoryApiPaths.query, 'get', data)
  }

  function enableCategory(uid: string) {
    return apiRequest(CategoryApiPaths.enable, 'post', { uid })
  }

  function disableCategory(uid: string) {
    return apiRequest(CategoryApiPaths.disable, 'post', { uid })
  }

  function findByParentId(parentId: string) {
    return queryCategory({ parentId: parentId, enabled: true })
  }

  return {
    addCategory,
    queryFirstLevelEnabledCategory,
    queryCategory,
    enableCategory,
    disableCategory,
    findByParentId,
  }
}
