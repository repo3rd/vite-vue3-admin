export interface AddCategoryData {
  name: string
  parentId?: string
  remark?: string
}

export interface CategoryData {
  uid: string
  name: string
  parentId: string
  parentName: string
  enabled: boolean
  createdAt: string
}

export interface SimpleCategoryData {
  uid: string
  name: string
}

export interface QueryCategoryData {
  name?: string
  parentId?: string
  enabled?: boolean
  leaf?: boolean
}
