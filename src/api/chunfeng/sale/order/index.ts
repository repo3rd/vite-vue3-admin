import { NewSaleOrderParam } from '@/api/chunfeng/sale/order/interfaces'
import { apiRequest } from '@/api/chunfeng/apiRequest'

enum SaleOrderApiPaths {
  new = '/sale/order/new',
}

export const useSaleOrderApi = () => {
  async function newSaleOrder(data: NewSaleOrderParam) {
    return apiRequest(SaleOrderApiPaths.new, 'post', data)
  }

  return {
    newSaleOrder,
  }
}
