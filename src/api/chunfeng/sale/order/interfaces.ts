import moment from 'moment'

export interface NewSaleOrderParam {
  customerId: string
  occurredAt: string
  items: NewSaleOrderItemParam[]
}

export interface NewSaleOrderItemParam {
  key: string
  productId: string
  brand: { name: string }
  unitId: string
  price: number
  quantity: number
  description?: string
}

export interface SimpleProductData {
  uid: string
  name: string
  brand: { name: string }
  description?: string
}

export interface SimpleUnitData {
  uid: string
  name: string
}
