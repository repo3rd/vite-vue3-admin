import moment from 'moment'
import { ProductPriceData } from '@/api/chunfeng/sale/price/interfaces'
import { PriceGroupData } from '@/api/chunfeng/sale/price-group/interfaces'

export interface AddCustomerProductPriceParam {
  customerId: string
  productPriceIds: string[]
  groupIds: string[]
}

export interface QueryCustomerProductPriceParam {
  customerId?: string
  productId?: string
  enabled?: boolean
}
//
// export interface CustomerProductPriceData {
//   uid: string
//   customerId: string
//   productPriceId: string
//   product: SimpleProductData
//   unit: SimpleData
//   priceGroup: SimpleData
//   price: number
//   enabled: boolean
//   createdAt: moment.Moment
// }

export interface CustomerProductPriceData {
  uid: string
  customerId: string
  productPrices: ProductPriceData[]
  priceGroups: PriceGroupData[]
}

interface SimpleProductData extends SimpleData {
  brandName: string
}

interface SimpleData {
  uid: string
  name: string
}
