import {
  AddCustomerProductPriceParam,
  CustomerProductPriceData,
  QueryCustomerProductPriceParam,
} from '@/api/chunfeng/sale/customer-price/interfaces'
import { apiRequest } from '@/api/chunfeng/apiRequest'

enum CustomerProductPriceApiPaths {
  add = '/sale/cpp/new',
  query = '/sale/cpp/query',
}

export const useCustomerProductPriceApi = () => {
  function addCustomerProductPrice(data: AddCustomerProductPriceParam) {
    return apiRequest(CustomerProductPriceApiPaths.add, 'post', data)
  }

  function queryCustomerProductPrice(data: QueryCustomerProductPriceParam) {
    return apiRequest<CustomerProductPriceData>(CustomerProductPriceApiPaths.query, 'get', data)
  }

  return {
    addCustomerProductPrice,
    queryCustomerProductPrice,
  }
}
