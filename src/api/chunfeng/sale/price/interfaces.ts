import moment from 'moment'
import { PriceGroupData } from '@/api/chunfeng/sale/price-group/interfaces'

export interface AddPriceParam {
  groupId: string
  productId: string
  unitId: string
  price: number
}

export interface QueryProductPriceParam {
  productId?: string
  unitId?: string
  groupId?: string
  enabled?: boolean
}

export interface ProductPriceData {
  uid: string
  productId: string
  productName: string
  brandId: string
  brandName: string
  unitId: string
  unitName: string
  price: number
  group: PriceGroupData
  enabled: boolean
  createdAt: moment.Moment
}
