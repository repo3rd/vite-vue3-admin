import { AddPriceParam, QueryProductPriceParam } from '@/api/chunfeng/sale/price/interfaces'
import { apiRequest } from '@/api/chunfeng/apiRequest'

enum PriceApiPaths {
  add = '/sale/price/add',
  batch_add = '/sale/price/batchadd',
  query = '/sale/price/query',
}

export const usePriceApi = () => {
  async function addPrice(data: AddPriceParam) {
    return apiRequest(PriceApiPaths.add, 'post', data)
  }

  async function batchAddPrice(data: AddPriceParam[]) {
    return apiRequest(PriceApiPaths.batch_add, 'post', data)
  }

  async function queryProductPrice(data: QueryProductPriceParam) {
    return apiRequest(PriceApiPaths.query, 'get', data)
  }

  return {
    addPrice,
    batchAddPrice,
    queryProductPrice,
  }
}
