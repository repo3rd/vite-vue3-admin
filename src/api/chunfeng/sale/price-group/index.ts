import {
  AddPriceGroupParam,
  QueryPriceGroupParam,
} from '@/api/chunfeng/sale/price-group/interfaces'
import { apiRequest } from '@/api/chunfeng/apiRequest'

enum PriceGroupApiPaths {
  add = '/sale/pricegroup/add',
  query = '/sale/pricegroup/query',
  disable = '/sale/pricegroup/disable',
  enable = '/sale/pricegroup/enable',
}

export const usePriceGroupApi = () => {
  function newPriceGroup(data: AddPriceGroupParam) {
    return apiRequest(PriceGroupApiPaths.add, 'post', data)
  }

  function disablePriceGroup(uid: string) {
    return apiRequest(PriceGroupApiPaths.disable + '/' + uid, 'post')
  }

  function enablePriceGroup(uid: string) {
    return apiRequest(PriceGroupApiPaths.enable + '/' + uid, 'post')
  }

  function queryPriceGroup(data: QueryPriceGroupParam) {
    return apiRequest(PriceGroupApiPaths.query, 'get', data)
  }

  return {
    newPriceGroup,
    disablePriceGroup,
    enablePriceGroup,
    queryPriceGroup,
  }
}
