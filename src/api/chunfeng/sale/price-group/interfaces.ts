import moment from 'moment'

export interface AddPriceGroupParam {
  name: string
}

export interface QueryPriceGroupParam {
  name?: string
  enabled?: boolean
}

export interface PriceGroupData {
  uid: string
  name: string
  enabled: boolean
  createdAt: moment.Moment
}
