import { NewReceiptParam, QueryReceiptParam } from '@/api/chunfeng/finance/receipt/interfaces'
import { apiRequest } from '@/api/chunfeng/apiRequest'

enum ReceiptApiPaths {
  add = '/receipt/new',
  query = '/receipt/query',
}

export const useReceiptApi = () => {
  function queryReceipt(data: QueryReceiptParam) {
    return apiRequest(ReceiptApiPaths.query, 'get', data)
  }

  function newReceipt(data: NewReceiptParam) {
    return apiRequest(ReceiptApiPaths.add, 'post', data)
  }

  return {
    queryReceipt,
    newReceipt,
  }
}
