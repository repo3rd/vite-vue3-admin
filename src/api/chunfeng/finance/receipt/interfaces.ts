import moment from 'moment'

export interface ReceiptData {
  uid: string
  customerId: string
  customerName: string
  amount: number
  occurredAt: string
}

export interface NewReceiptParam {
  customerId: string
  amount: Number
  occurredAt: string
}

export interface QueryReceiptParam {
  customerId?: string
  occurredStart?: moment.Moment
  occurredEnd?: moment.Moment
}
