import moment from 'moment'

export interface QueryArParam {
  customerId?: string
  status?: ArStatus
}

export interface ArData {
  uid: string
  customerId: string
  customerName: string
  sourceId: string
  amount: number
  unconfirmed: number
  status: ArStatus
  occurredAt: moment.Moment
  createdAt: moment.Moment
}

type ArStatus = 'NEW' | 'PART' | 'DONE'
