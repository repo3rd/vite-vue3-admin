import { ArData, QueryArParam } from '@/api/chunfeng/finance/ar/interfaces'
import { apiRequest } from '@/api/chunfeng/apiRequest'

enum ArApiPaths {
  query = '/ar/query',
}

export const useArApi = () => {
  function queryAr(data: QueryArParam) {
    return apiRequest<ArData>(ArApiPaths.query, 'get', data)
  }

  return {
    queryAr,
  }
}
