import { NewExpenseParam, QueryExpenseParam } from '@/api/chunfeng/finance/expense/interfaces'
import { apiRequest } from '@/api/chunfeng/apiRequest'

enum ExpenseApiPaths {
  add = '/expense/new',
  query = '/expense/query',
}

export const useExpenseApi = () => {
  function newExpense(data: NewExpenseParam) {
    return apiRequest(ExpenseApiPaths.add, 'post', data)
  }

  function queryExpense(data: QueryExpenseParam) {
    return apiRequest(ExpenseApiPaths.query, 'get', data)
  }

  return {
    newExpense,
    queryExpense,
  }
}
