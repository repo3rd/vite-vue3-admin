import moment from 'moment'

export interface NewExpenseParam {
  name: string
  expenseTypeId: string
  amount: number
  remark?: string
  occurredAt: string
}

export interface QueryExpenseParam {
  name?: string
  expenseTypeId?: string
  occurredStart?: moment.Moment
  occurredEnd?: moment.Moment
}

export interface ExpenseData {
  uid: string
  expenseTypeId: string
  expenseTypeName: string
  amount: number
  occurredAt: moment.Moment
  remark?: string
  createdAt: moment.Moment
}
