import moment from 'moment'

export interface NewExpenseTypeParam {
  name: string
  remark?: string
}

export interface QueryExpenseTypeParam {
  name?: string
  enabled?: boolean
}

export interface ExpenseTypeData {
  uid: string
  name: string
  enabled: boolean
  remark?: string
  createdAt: moment.Moment
}
