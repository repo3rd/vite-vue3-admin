import {
  NewExpenseTypeParam,
  QueryExpenseTypeParam,
} from '@/api/chunfeng/finance/expense-type/interfaces'
import { apiRequest } from '@/api/chunfeng/apiRequest'

enum ExpenseTypeApiPaths {
  add = '/expenseType/new',
  query = '/expenseType/query',
}

export const useExpenseTypeApi = () => {
  function newExpenseType(data: NewExpenseTypeParam) {
    return apiRequest(ExpenseTypeApiPaths.add, 'post', data)
  }

  function queryExpenseType(data: QueryExpenseTypeParam) {
    return apiRequest(ExpenseTypeApiPaths.query, 'get', data)
  }

  return {
    newExpenseType,
    queryExpenseType,
  }
}
