export interface ApiResult<T = any> {
  success: boolean
  data?: T
  status: ApiResultStatus
}

export interface ApiResultStatus {
  code: string
  message: string
}

export type SuccessCallback = () => void
export type QueryResultCallback<T = any> = (data: T) => void
export type CommandResultCallback = () => void
export type FailCallback = (status: ApiResultStatus) => void

export type ApiFunc<T> = Promise<T>

export interface CommandSuccessCallbackOption {
  message?: string
  callback: () => void
}
