import { storage } from '@/utils/Storage'

export const KEY_TOKEN = 'CF_TOKEN'

function getAuthToken() {
  return storage.get(KEY_TOKEN)
}

function removeAuthToken() {
  storage.remove(KEY_TOKEN)
}

function setAuthToken(token: string) {
  storage.set(KEY_TOKEN, token)
}

export const useAuthTool = () => {
  return {
    getAuthToken,
    removeAuthToken,
    setAuthToken
  }
}
