interface CategoryTreeData {
  value: string
  key: string
  label: string
  children?: CategoryTreeData[]
}

export const useCategorySelectionFunctions = () => {
  function loadData(uid: string): CategoryTreeData {
    return { value: '', key: '', label: '' }
  }

  return {
    loadData,
  }
}
